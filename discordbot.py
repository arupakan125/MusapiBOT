import discord
import datetime
import asyncio
import os
import requests
from musapi import minecraft
from musapi import bokete

client = discord.Client()


@client.event
async def on_ready():
    print('ログインしました')
    await client.change_presence(activity=discord.Game(name='A-Zタイピング'))


@client.event
async def on_message(message):
    if message.content.startswith('/debug') and os.environ["MUSAPIBOT_CMD_DEBUG"] == "True":
        await message.channel.send('debug msg')
        await message.channel.send('タイピングしよ')
        await message.channel.send(file=discord.File('musapi/typing.png'))
    if message.content.startswith('/bokete') and os.environ["MUSAPIBOT_CMD_BOKETE"] == "True":
        await message.channel.send(bokete.get_odai())
        await message.channel.send("この画像で面白いこと言って！！")
    if message.content.startswith('/test') and os.environ["MUSAPIBOT_CMD_TEST"] == "True":
        await message.channel.send("サーバの様子を見てくるね......")
        code = minecraft.check_port("play.crafters.love", 25565)
        if code == 0:
            await message.channel.send("[OK] サーバは落ちていないように見えるよ！")
        else:
            await message.channel.send("[NG] サーバ落ちてる！！！")

    if os.environ.get("MUSAPIBOT_TALK_APIKEY") is not None:
        if client.user in message.mentions:
            files = {
                'apikey': (None, os.environ["MUSAPIBOT_TALK_APIKEY"]),
                'query': (None, message.content.strip("<@542280433497407490>").strip()),
            }
            response = requests.post(
                'https://api.a3rt.recruit-tech.co.jp/talk/v1/smalltalk', files=files).json()

            reply = message.author.mention + " " + \
                response["results"][0]["reply"]
            await message.channel.send(reply)


async def main_loop():
    await client.wait_until_ready()
    ch_maebaru = client.get_channel(446234923779489793)
    ch_meinohama = client.get_channel(446234975113707525)
    ch_gakken = client.get_channel(485828672335642624)
    ch_hakozaki = client.get_channel(485828705227243525)
    ch_tenjin = client.get_channel(434653372923248651)
    ch_onojo = client.get_channel(486426464993148959)
    ch_nakagawa = client.get_channel(486426554742603777)
    ch_sarayama = client.get_channel(487172491257577472)
    ch_dazaifu = client.get_channel(505910670458486804)
    ch_kasuga = client.get_channel(505910715530608661)
    ch_chikushino = client.get_channel(505910782056464384)
    ch_ohashi = client.get_channel(614387065525764096)
    role_maebaru = '<@&448768645242290176> '
    role_meinohama = '<@&448768717585645569> '
    role_gakken = '<@&448768743124762624> '
    role_tenjin = '<@&503065428793491457> '
    role_hakozaki = '<@&502044108085526528> '
    role_onojo = '<@&503127169006829570> '
    role_nakagawa = '<@&503127207875444747> '
    role_sarayama = '<@&503127250929975317> '
    role_kasuga = '<@&508510424493457418> '
    role_dazaifu = '<@&506744094786060288> '
    role_chikushino = '<@&508510570211966986> '
    role_ohashi = '<@&614387266080604160> '

    while True:
        JST = datetime.timezone(datetime.timedelta(hours=+9), 'JST')
        time = datetime.datetime.now(JST).strftime("%H:%M")
        weekday = datetime.datetime.now().strftime("%a")
        print("debug: " + datetime.datetime.now(JST).strftime("%H:%M %a"))

        # --- 大野城ラボ -------------------------------------------------------#
        # 火曜日、水曜日、金曜日
        if weekday == "Tue" or weekday == "Wed" or weekday == "Fri":
            if time == "17:00":
                asyncio.ensure_future(sendMessage(ch_onojo, role_onojo))
            if time == "19:00":
                asyncio.ensure_future(sendMessage(ch_onojo, role_onojo))
            if time == "18:25":
                asyncio.ensure_future(sendEndMessage(ch_onojo, role_onojo))
            if time == "20:25":
                asyncio.ensure_future(sendEndMessage(ch_onojo, role_onojo))
        # 土曜日
        if weekday == "Sat":
            if time == "10:00":
                asyncio.ensure_future(sendMessage(ch_onojo, role_onojo))
            if time == "13:00":
                asyncio.ensure_future(sendMessage(ch_onojo, role_onojo))
            if time == "11:25":
                asyncio.ensure_future(sendEndMessage(ch_onojo, role_onojo))
            if time == "14:25":
                asyncio.ensure_future(sendEndMessage(ch_onojo, role_onojo))

        # --- 那珂川ラボ -------------------------------------------------------#
        # 水曜日、土曜日
        if weekday == "Wed" or weekday == "Sat":
            if time == "17:00":
                asyncio.ensure_future(sendMessage(ch_nakagawa, role_nakagawa))
            if time == "19:00":
                asyncio.ensure_future(sendMessage(ch_nakagawa, role_nakagawa))
            if time == "18:25":
                asyncio.ensure_future(
                    sendEndMessage(ch_nakagawa, role_nakagawa))
            if time == "20:25":
                asyncio.ensure_future(
                    sendEndMessage(ch_nakagawa, role_nakagawa))

        # --- 天神ラボ ---------------------------------------------------------#
        # 土曜日
        if weekday == "Sat":
            if time == "10:00":
                asyncio.ensure_future(sendMessage(ch_tenjin, role_tenjin))
            if time == "13:00":
                asyncio.ensure_future(sendMessage(ch_tenjin, role_tenjin))
            if time == "15:00":
                asyncio.ensure_future(sendMessage(ch_tenjin, role_tenjin))
            if time == "17:00":
                asyncio.ensure_future(sendMessage(ch_tenjin, role_tenjin))
            if time == "11:25":
                asyncio.ensure_future(sendEndMessage(ch_tenjin, role_tenjin))
            if time == "14:25":
                asyncio.ensure_future(sendEndMessage(ch_tenjin, role_tenjin))
            if time == "16:25":
                asyncio.ensure_future(sendEndMessage(ch_tenjin, role_tenjin))
            if time == "18:25":
                asyncio.ensure_future(sendEndMessage(ch_tenjin, role_tenjin))

        # --- 姪浜ラボ ---------------------------------------------------------#
        # 土曜日
        if weekday == "Thu":
            if time == "17:30":
                asyncio.ensure_future(sendMessage(
                    ch_meinohama, role_meinohama))
            if time == "19:15":
                asyncio.ensure_future(sendMessage(
                    ch_meinohama, role_meinohama))
            if time == "18:55":
                asyncio.ensure_future(sendEndMessage(
                    ch_meinohama, role_meinohama))
            if time == "20:40":
                asyncio.ensure_future(sendEndMessage(
                    ch_meinohama, role_meinohama))

        # --- 前原ラボ ---------------------------------------------------------#
        # 水曜日
        if weekday == "Wed":
            if time == "17:00":
                asyncio.ensure_future(sendMessage(ch_maebaru, role_maebaru))
            if time == "18:45":
                asyncio.ensure_future(sendMessage(ch_maebaru, role_maebaru))
            if time == "18:25":
                asyncio.ensure_future(sendEndMessage(ch_maebaru, role_maebaru))
            if time == "20:10":
                asyncio.ensure_future(sendEndMessage(ch_maebaru, role_maebaru))

        # --- 箱崎ラボ ---------------------------------------------------------#
        # 金曜日
        if weekday == "Fri":
            if time == "17:00":
                asyncio.ensure_future(sendMessage(ch_hakozaki, role_hakozaki))
            if time == "19:00":
                asyncio.ensure_future(sendMessage(ch_hakozaki, role_hakozaki))
            if time == "18:25":
                asyncio.ensure_future(
                    sendEndMessage(ch_hakozaki, role_hakozaki))
            if time == "20:25":
                asyncio.ensure_future(
                    sendEndMessage(ch_hakozaki, role_hakozaki))

        # --- 皿山ラボ ---------------------------------------------------------#
        # 木曜日
        if weekday == "Thu":
            if time == "17:00":
                asyncio.ensure_future(sendMessage(ch_sarayama, role_sarayama))
            if time == "19:00":
                asyncio.ensure_future(sendMessage(ch_sarayama, role_sarayama))
            if time == "18:25":
                asyncio.ensure_future(
                    sendEndMessage(ch_sarayama, role_sarayama))
            if time == "20:25":
                asyncio.ensure_future(
                    sendEndMessage(ch_sarayama, role_sarayama))

        # --- 九大学研都市ラボ -------------------------------------------------#
        # 土曜日
        if weekday == "Sat":
            if time == "10:00":
                asyncio.ensure_future(sendMessage(ch_gakken, role_gakken))
        if weekday == "Sat":
            if time == "11:25":
                asyncio.ensure_future(sendEndMessage(ch_gakken, role_gakken))

        # --- 太宰府ラボ -------------------------------------------------------#
        # 木曜日
        if weekday == "Thu":
            if time == "18:30":
                asyncio.ensure_future(sendMessage(ch_dazaifu, role_dazaifu))
            if time == "19:55":
                asyncio.ensure_future(sendEndMessage(ch_dazaifu, role_dazaifu))
        # 日曜日
        if weekday == "Sun":
            if time == "10:00":
                asyncio.ensure_future(sendMessage(ch_dazaifu, role_dazaifu))
            if time == "11:25":
                asyncio.ensure_future(sendEndMessage(ch_dazaifu, role_dazaifu))

        # --- 筑紫野ラボ -------------------------------------------------------#
        # 水曜日
        if weekday == "Wed":
            if time == "18:30":
                asyncio.ensure_future(sendMessage(
                    ch_chikushino, role_chikushino))
            if time == "20:55":
                asyncio.ensure_future(sendEndMessage(
                    ch_chikushino, role_chikushino))
        # 土曜日
        if weekday == "Sat":
            if time == "10:00":
                asyncio.ensure_future(sendMessage(
                    ch_chikushino, role_chikushino))
            if time == "11:25":
                asyncio.ensure_future(sendEndMessage(
                    ch_chikushino, role_chikushino))

        # --- 春日ラボ ---------------------------------------------------------#
        # 火曜日
        if weekday == "Tue":
            if time == "18:30":
                asyncio.ensure_future(sendMessage(ch_kasuga, role_kasuga))
            if time == "20:55":
                asyncio.ensure_future(sendEndMessage(ch_kasuga, role_kasuga))
        # 土曜日
        if weekday == "Sat":
            if time == "17:00":
                asyncio.ensure_future(sendMessage(ch_kasuga, role_kasuga))
            if time == "18:25":
                asyncio.ensure_future(sendEndMessage(ch_kasuga, role_kasuga))

        # --- 大橋ラボ ---------------------------------------------------------#
        # 水曜日、金曜日
        if weekday == "Wed" or weekday == "Fri":
            if time == "17:00":
                asyncio.ensure_future(sendMessage(ch_ohashi, role_ohashi))
            if time == "18:00":
                asyncio.ensure_future(sendMessage(ch_ohashi, role_ohashi))
            if time == "18:25":
                asyncio.ensure_future(sendEndMessage(ch_ohashi, role_ohashi))
            if time == "19:25":
                asyncio.ensure_future(sendEndMessage(ch_ohashi, role_ohashi))

        await asyncio.sleep(60)


async def mc_test_loop():
    await client.wait_until_ready()
    ch_notify = client.get_channel(525595899309588490)
    previous = True
    count = 0

    while True:
        status = minecraft.check_port("play.crafters.love", 25565)
        if status == 0:
            count = 0
            if previous == False:
                await ch_notify.send("復旧したっぽい！")
                print("send ok")
                previous = True
        if status != 0:
            print("NG")
            count = count + 1
            print(count)
            if count == 3:
                if previous == True:
                    print("send ng")
                    await ch_notify.send("<@236757663181045761> サーバ落ちたみたい")
                    previous = False
        await asyncio.sleep(3)


async def sendMessage(channel, role):
    await channel.send(role + ' タイピングしよ')
    await channel.send(file=discord.File('musapi/typing.png'))


async def sendEndMessage(channel, role):
    await channel.send(role + ' 感想書いて')
    await channel.send(file=discord.File('musapi/kanso.png'))

if os.environ["MUSAPIBOT_LOOP_MESSAGE"] == "True":
    client.loop.create_task(main_loop())
if os.environ["MUSAPIBOT_LOOP_MC"] == "True":
    client.loop.create_task(mc_test_loop())
token = os.environ["MUSAPIBOT_TOKEN"]
client.run(token)
