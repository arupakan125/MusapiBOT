import socket


def check_port(target, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(5)
    try:
        code = sock.connect_ex((target, port))
    except socket.gaierror:
        return 2
    sock.close()
    if code == 0:
        return 0
    else:
        return 1
