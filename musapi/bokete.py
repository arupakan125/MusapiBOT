import requests
import random
from bs4 import BeautifulSoup


def get_odai():
    i = random.randrange(1, 50, 1)
    odai = []
    odai_response = requests.get(
        "https://bokete.jp/odai/popular?page=" + str(i)).text
    odai_soup = BeautifulSoup(odai_response, "html.parser")
    for img in odai_soup.select("div.odai-photo-wrapper > a > img"):
        odai.append("https:" + img["src"])
    return random.choice(odai)
